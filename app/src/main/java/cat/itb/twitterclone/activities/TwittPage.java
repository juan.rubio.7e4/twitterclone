package cat.itb.twitterclone.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.appbar.MaterialToolbar;
import com.squareup.picasso.Picasso;

import cat.itb.twitterclone.R;
import cat.itb.twitterclone.models.Twitt;

public class TwittPage extends AppCompatActivity {

    ImageView userImage;
    TextView username;
    TextView alias;
    TextView twittMessage;
    TextView retwitt;
    TextView like;
    TextView date;
    MaterialToolbar topAppBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitt_page);

        userImage = findViewById(R.id.imageUserTwittPage);
        username = findViewById(R.id.usernameTwittPage);
        alias = findViewById(R.id.aliasTwittPage);
        twittMessage = findViewById(R.id.messageTwittPage);
        retwitt = findViewById(R.id.retwittTwittPage);
        like = findViewById(R.id.likesTwittPage);
        date = findViewById(R.id.hourTwittPage);
        topAppBar = findViewById(R.id.topAppBarTwittPage);

        final Bundle bundle = getIntent().getExtras();

        String userImage = bundle.getString("image");
        String username = bundle.getString("username");
        String alias = bundle.getString("alias");
        String twitt = bundle.getString("twitt");
        int retwitt = bundle.getInt("retwitt");
        int like = bundle.getInt("like");
        String date = bundle.getString("date");


        Picasso.with(this).load(userImage).into(this.userImage);
        this.username.setText(username);
        this.alias.setText(alias);
        this.twittMessage.setText(twitt);
        this.retwitt.setText(retwitt + " " + getApplicationContext().getResources().getText(R.string.retwitt));
        this.like.setText(like + " " +  getApplicationContext().getResources().getText(R.string.like));
        this.date.setText(date);

        if (Build.VERSION.SDK_INT >= 21) {

            Slide transition = new Slide();
            transition.setDuration(300);
            transition.setSlideEdge(Gravity.RIGHT);
            getWindow().setReturnTransition(transition);
            getWindow().setEnterTransition(transition);
            getWindow().setExitTransition(transition);
            getWindow().setReenterTransition(transition);
        }

        topAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TwittPage.this, MainActivity.class);
                intent.putExtra("email", bundle.getString("email"));
                startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(TwittPage.this).toBundle());
            }
        });
    }
}