package cat.itb.twitterclone.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cat.itb.twitterclone.R;
import cat.itb.twitterclone.models.TwittViewModel;
import cat.itb.twitterclone.models.User;

public class Register extends AppCompatActivity {

    Button register;
    MaterialToolbar topAppBar;

    TextView usernameError;
    TextView usernameSize;
    TextView phoneError;
    EditText phoneEditText;
    EditText birthDateEditText;
    EditText usernameEditText;

    boolean usernameValid = false;
    boolean phoneValid = false;
    boolean birthDateValid = false;

    TwittViewModel usersViewModel;
    User user = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        usersViewModel = new ViewModelProvider(this).get(TwittViewModel.class);

        register = findViewById(R.id.buttonRegister2);
        topAppBar = findViewById(R.id.topAppBarRegister);

        phoneEditText = findViewById(R.id.phoneEditText);
        birthDateEditText = findViewById(R.id.birthDateEditText);
        usernameEditText = findViewById(R.id.usernameEditText);
        usernameError = findViewById(R.id.errorUsername);
        usernameSize = findViewById(R.id.sizeUsername);
        phoneError = findViewById(R.id.phoneEmail);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usernameValid && phoneValid && birthDateValid){
                    user.setUsername(usernameEditText.getText().toString());
                    user.setPassword(phoneEditText.getText().toString());
                    usersViewModel.setUser(user);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }else {
                    Toast.makeText(getApplicationContext(), R.string.registerFail, Toast.LENGTH_SHORT).show();
                }
            }
        });

        topAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Hello.class);
                startActivity(intent);
            }
        });

        usernameEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP){
                    if (usernameEditText.getText().toString().length() <= 50){
                        usernameEditText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_check_circle_outline_24,0);
                        usernameEditText.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
                        usernameError.setVisibility(View.INVISIBLE);
                        usernameValid = true;
                    }else {
                        usernameEditText.getBackground().setColorFilter(getResources().getColor(R.color.errorColor), PorterDuff.Mode.SRC_ATOP);
                        usernameEditText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                        usernameError.setVisibility(View.VISIBLE);
                        usernameValid = false;
                    }
                }
                return false;
            }
        });

        phoneEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (KeyEvent.ACTION_UP== event.getAction()){
                    if (phoneEditText.getText().toString().length() == 9){
                        phoneEditText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_check_circle_outline_24,0);
                        phoneEditText.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
                        phoneError.setVisibility(View.INVISIBLE);
                        phoneValid = true;
                    }else {
                        phoneError.setVisibility(View.VISIBLE);
                        phoneEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0 , 0, 0);
                        phoneEditText.getBackground().setColorFilter(getResources().getColor(R.color.errorColor), PorterDuff.Mode.SRC_ATOP);
                        phoneValid = false;
                    }

                }
                return false;
            }
        });

        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText("Select your Birth Date");
        builder.setTheme(R.style.ThemeOverlay_App_DatePicker);
        final MaterialDatePicker<Long> materialDatePicker = builder.build();

        birthDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                materialDatePicker.show(getSupportFragmentManager(), "MATERIAL_DATE_PICKER");
                materialDatePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
                    @Override
                    public void onPositiveButtonClick(Long selection) {
                        Date date = new Date(selection);


                        DateFormat format =new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                        String localDate=format.format(date);
                        user.setBirthDate(date);
                        birthDateEditText.setText(localDate);
                        birthDateValid = true;
                    }
                });

            }
        });
    }
}