package cat.itb.twitterclone.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.List;

import cat.itb.twitterclone.R;
import cat.itb.twitterclone.models.TwittViewModel;
import cat.itb.twitterclone.models.User;

public class SignIn extends AppCompatActivity {

    MaterialToolbar topAppBar;
    MaterialButton register;
    Button signIn;

    EditText usernameEditText;
    EditText passwordEditText;
    TwittViewModel usersViewModel;
    SignInButton loginGoogle;

    boolean passwordShown = false;
    private static final int RC_SIGN_IN = 100;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        usersViewModel = new ViewModelProvider(this).get(TwittViewModel.class);
        final List<User> users = usersViewModel.getUsers();
        topAppBar = findViewById(R.id.topAppBarSignIn);
        register = findViewById(R.id.buttonRegister);
        signIn = findViewById(R.id.buttonSignIn);
        loginGoogle = findViewById(R.id.sign_in_button_google);

        usernameEditText = findViewById(R.id.editTextUserSignIn);
        passwordEditText = findViewById(R.id.editTextPasswordSignIn);

        topAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignIn.this, Hello.class);
                startActivity(intent);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignIn.this, Register.class);
                startActivity(intent);
            }
        });

        passwordEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_UP){
                    if (event.getRawX() >= (passwordEditText.getRight() - passwordEditText.getCompoundDrawables()[2].getBounds().width())){
                        if (passwordShown){
                            passwordShown = false;
                            passwordEditText.setInputType(129);

                            passwordEditText.setTypeface(Typeface.SANS_SERIF);
                            passwordEditText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_eye_grey_24,0);
                        }else {
                            passwordShown = true;
                            passwordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            passwordEditText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_remove_red_eye_24,0);
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean userValid = false;
                String username = "@" + usernameEditText.getText().toString();
                for (User u: users) {
                    if (username.equals(u.getUsername()) &&
                    passwordEditText.getText().toString().equals(u.getPassword())){
                        usersViewModel.setUser(u);
                        Intent intent = new Intent(SignIn.this, MainActivity.class);
                        startActivity(intent);
                        userValid = true;
                    }
                }
                if (!userValid){
                    Toast.makeText(getApplicationContext(), R.string.loginFail , Toast.LENGTH_LONG).show();
                }
            }
        });


        loginGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Configure Google Sign In
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getString(R.string.default_web_client_id))
                        .requestEmail()
                        .build();

                GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(SignIn.this, gso);
                googleSignInClient.signOut();

                Intent signInIntent = googleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN );
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            final GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            if (account != null){
                AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
                FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Intent intent = new Intent(SignIn.this, MainActivity.class);
                            intent.putExtra("email", account.getEmail());
                            startActivity(intent);
                        }else {
                            Toast.makeText(getApplicationContext(), "No se ha podido iniciar sesion", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Toast.makeText( getApplicationContext(), "signInResult:failed code=" + e.getStatusCode() + " Mensaje: " + e.getLocalizedMessage() + ". Causa: "+ e.getCause(), Toast.LENGTH_SHORT).show();
        }
    }
}