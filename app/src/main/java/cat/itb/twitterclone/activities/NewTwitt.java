package cat.itb.twitterclone.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.android.material.button.MaterialButton;
import com.squareup.picasso.Picasso;

import java.util.Date;

import cat.itb.twitterclone.R;
import cat.itb.twitterclone.fragments.Home;
import cat.itb.twitterclone.models.Twitt;
import cat.itb.twitterclone.models.TwittViewModel;
import cat.itb.twitterclone.models.User;

public class NewTwitt extends AppCompatActivity {

    ImageButton close;
    MaterialButton send;
    EditText message;
    ImageView imageUser;
    TwittViewModel twittViewModel;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_twitt);

        bundle = getIntent().getExtras();

        twittViewModel = new ViewModelProvider(this).get(TwittViewModel.class);

        close = findViewById(R.id.buttonClose);
        send = findViewById(R.id.buttonTwitt);
        message = findViewById(R.id.editText);
        imageUser = findViewById(R.id.imageUserNewTwitt);

        Picasso.with(getApplicationContext()).load(twittViewModel.getUser().getImage()).into(imageUser);

        if (Build.VERSION.SDK_INT >= 21) {
            Transition transitionEnter = new Slide();
            transitionEnter.setDuration(300);
            getWindow().setEnterTransition(transitionEnter);
            getWindow().setExitTransition(transitionEnter);
            getWindow().setReturnTransition(transitionEnter);
            getWindow().setReenterTransition(transitionEnter);
        }

        close.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewTwitt.this, MainActivity.class);
                intent.putExtra("email", bundle.getString("email"));
                startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(NewTwitt.this).toBundle());
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewTwitt.this, MainActivity.class);
                intent.putExtra("email", bundle.getString("email"));
                Twitt twitt = new Twitt(message.getText().toString(), new Date(), twittViewModel.getUser());
                twittViewModel.subirTwitt(twitt);

                startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(NewTwitt.this).toBundle());
            }
        });
    }
}