package cat.itb.twitterclone.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cat.itb.twitterclone.R;

public class Hello extends AppCompatActivity {

    Button register;
    TextView signIn;
    RelativeLayout helloLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello);

        helloLayout = findViewById(R.id.helloLayout);

        SharedPreferences preferences = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE);
        String email = preferences.getString("email", null);
        //Si la sesion esta abierta, pasamos al MainActivity
        if (email != null){
            helloLayout.setVisibility(View.INVISIBLE);
            Intent intent = new Intent(Hello.this, MainActivity.class);
            intent.putExtra("email", email);
            startActivity(intent);
        }

        register = findViewById(R.id.buttonRegister);
        signIn = findViewById(R.id.logIn);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Hello.this, Register.class);
                startActivity(intent);
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Hello.this, SignIn.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        helloLayout.setVisibility(View.VISIBLE);
    }
}