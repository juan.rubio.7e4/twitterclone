package cat.itb.twitterclone.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.common.util.Strings;
import com.google.android.material.button.MaterialButton;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cat.itb.twitterclone.R;
import cat.itb.twitterclone.fragments.Home;
import cat.itb.twitterclone.models.Twitt;
import cat.itb.twitterclone.models.TwittViewModel;
import cat.itb.twitterclone.models.User;

public class TwittAdapter extends FirebaseRecyclerAdapter<Twitt, TwittAdapter.TwittViewHolder> {
    User user;
    OnItemClickListener itemClickListener;
    Context context;

    public interface OnItemClickListener {
        void onItemClick(Twitt twitt , int position);
    }

    public TwittAdapter(@NonNull FirebaseRecyclerOptions<Twitt> options, User user, Context context, OnItemClickListener listener) {
        super(options);
        this.user = user;
        this.itemClickListener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public TwittViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        return new TwittViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull TwittViewHolder holder, int position, @NonNull Twitt model) {
        holder.bind(model, this.itemClickListener);
    }

    class TwittViewHolder extends RecyclerView.ViewHolder{

        ImageView imageUser;
        TextView alias;
        TextView username;
        TextView hour;
        TextView twittText;
        MaterialButton messages;
        MaterialButton retwitt;
        LottieAnimationView like;
        TextView likeTextView;

        public TwittViewHolder(@NonNull View itemView) {
            super(itemView);

            imageUser = itemView.findViewById(R.id.imageUser);
            alias = itemView.findViewById(R.id.alias);
            username = itemView.findViewById(R.id.username);
            hour = itemView.findViewById(R.id.hour);
            twittText = itemView.findViewById(R.id.twitt);
            messages = itemView.findViewById(R.id.messages);
            retwitt = itemView.findViewById(R.id.retwitt);
            like = itemView.findViewById(R.id.likeLottie);
            likeTextView = itemView.findViewById(R.id.likeTextView);
        }

        public void bind (final Twitt twitt, final OnItemClickListener listener){
            Picasso.with(context).load(twitt.getUser().getImage()).into(imageUser);
            alias.setText(twitt.getUser().getAlias());
            username.setText(twitt.getUser().getUsername());
            String publicationTime = time(twitt.getDate());
            hour.setText(publicationTime);
            twittText.setText(twitt.getText());
            messages.setText(twitt.getMessage() + "");
            retwitt.setText(twitt.getRetwitt() + "");
            if (twitt.getUsersLike() == null){
                twitt.setUsersLike(new ArrayList<String>());
            }
            likeTextView.setText(twitt.getUsersLike().size() + "");

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(twitt, getAdapterPosition());
                }
            });

            startStatusLikeAnimation(twitt);

            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (twitt.userLiked(user)){
                        likeDown(twitt);
                    }else{
                        likeUp(twitt);
                    }
                }
            });
        }

        public void likeUp(Twitt twitt){
            twitt.getUsersLike().add(user.getUsername());
            TwittViewModel.actualizarLikesTwitt(twitt);
            like.setAnimation(R.raw.like_animation);
            like.playAnimation();
        }

        public void likeDown(Twitt twitt){
            twitt.getUsersLike().remove(user.getUsername());
            TwittViewModel.actualizarLikesTwitt(twitt);
            like.setImageResource(R.drawable.twitter_like);
        }

        public void startStatusLikeAnimation(Twitt twitt){
            for (String username: twitt.getUsersLike()) {
                if (username.equals(user.getUsername())){
                    like.setAnimation(R.raw.like_animation);
                    like.playAnimation();
                    break;
                }else {
                    like.setImageResource(R.drawable.twitter_like);
                }
            }
            if (twitt.getUsersLike().isEmpty()) {
                like.setImageResource(R.drawable.twitter_like);
            }
        }

        public String time (Date date){
            Date actualTime = new Date();
            String publicationTime;

            int publicationTimeInt = (int) (actualTime.getTime() - date.getTime());
            if (publicationTimeInt < 3600000){
                publicationTimeInt = publicationTimeInt / 60000;
                publicationTime = publicationTimeInt + " min.";
            }else {
                publicationTimeInt = publicationTimeInt / 3600000;
                publicationTime = publicationTimeInt + " h.";
            }

            return publicationTime;
        }
    }
}
