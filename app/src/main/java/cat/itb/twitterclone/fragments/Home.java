package cat.itb.twitterclone.fragments;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import cat.itb.twitterclone.R;
import cat.itb.twitterclone.activities.TwittPage;
import cat.itb.twitterclone.adapters.TwittAdapter;
import cat.itb.twitterclone.models.Twitt;
import cat.itb.twitterclone.models.TwittViewModel;
import cat.itb.twitterclone.models.User;

public class Home extends Fragment {
    RecyclerView recyclerView;
    TwittAdapter adapter;
    TwittViewModel twittViewModel;
    User user;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        twittViewModel = new ViewModelProvider(this).get(TwittViewModel.class);

        recyclerView = view.findViewById(R.id.recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration divider = new DividerItemDecoration(getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(divider);

        final FirebaseRecyclerOptions<Twitt> options = new FirebaseRecyclerOptions.Builder<Twitt>()
                .setQuery(twittViewModel.getMyRefTwitts(), Twitt.class).build();

        Bundle bundle = getActivity().getIntent().getExtras();

        user = new User();
        final String email = bundle.getString("email");
        String formatEmail = email.replace(".","-");

        twittViewModel.getMyRefUsers().child(formatEmail).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    user.setEmail(email);
                    user.setUsername(snapshot.child("username").getValue().toString());
                    user.setImage(snapshot.child("image").getValue().toString());
                    user.setAlias(snapshot.child("alias").getValue().toString());
                    user.setPassword(snapshot.child("password").getValue().toString());
                    user.setLikeTwitts((List<Twitt>)snapshot.child("likeTwitts").getValue());
                    twittViewModel.setUser(user);
                }else {
                    user.setEmail(email);
                    user.setPassword("123456");
                    String username = email.split("@")[0];
                    user.setAlias(username);
                    user.setUsername(username);
                    user.setImage("https://firebasestorage.googleapis.com/v0/b/twitterclone-e1f38.appspot.com/o/img_users%2Ftwitter_circle_blue.png?alt=media&token=01adea2f-ef4a-428c-a16b-d286a5a9bb54");
                    twittViewModel.setUser(user);
                    twittViewModel.subirUser(user);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        twittViewModel.setTwitts(options.getSnapshots());

        adapter = new TwittAdapter(options, user, getContext() , new TwittAdapter.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onItemClick(Twitt twitt, int position) {
                Intent intent = new Intent(getContext(), TwittPage.class);
                intent.putExtra("username", twitt.getUser().getUsername());
                intent.putExtra("alias", twitt.getUser().getAlias());
                intent.putExtra("image", twitt.getUser().getImage());
                intent.putExtra("message", twitt.getMessage());
                intent.putExtra("retwitt", twitt.getRetwitt());
                intent.putExtra("like", twitt.getUsersLike().size());
                intent.putExtra("twitt", twitt.getText());
                intent.putExtra("date", twitt.getDate().toString());
                intent.putExtra("email", user.getEmail());

                for (Twitt t: twittViewModel.getTwitts()) {
                    twittViewModel.actualizarLikesTwitt(t);
                }

                startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
            }
        });
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();

    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
