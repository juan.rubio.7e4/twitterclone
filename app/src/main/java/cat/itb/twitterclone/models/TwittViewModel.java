package cat.itb.twitterclone.models;

import androidx.lifecycle.ViewModel;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TwittViewModel extends ViewModel {

    private FirebaseDatabase database;
    private static DatabaseReference myRefUsers;
    private static DatabaseReference myRefTwitts;
    private StorageReference storageReference;

    private static List<Twitt> twitts = new ArrayList<>();
    private static List<User> users = new ArrayList<>();
    private static User user ;

    public TwittViewModel (){

        database = FirebaseDatabase.getInstance();
        myRefUsers = database.getReference().child("Users");
        myRefTwitts = database.getReference().child("Twitts");
        storageReference = FirebaseStorage.getInstance().getReference().child("img_users");

    }

    public List<Twitt> getTwitts() {
        return twitts;
    }

    public void setTwitts(List<Twitt> twitts) {
        this.twitts = twitts;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void addTwitt(Twitt twitt){
        twitts.add(twitt);
    }

    public static List<User> getUsers() {
        return users;
    }

    public static void setUsers(List<User> users) {
        TwittViewModel.users = users;
    }

    public FirebaseDatabase getDatabase() {
        return database;
    }

    public void setDatabase(FirebaseDatabase database) {
        this.database = database;
    }

    public DatabaseReference getMyRefUsers() {
        return myRefUsers;
    }

    public void setMyRefUsers(DatabaseReference myRefUsers) {
        this.myRefUsers = myRefUsers;
    }

    public DatabaseReference getMyRefTwitts() {
        return myRefTwitts;
    }

    public void setMyRefTwitts(DatabaseReference myRefTwitts) {
        this.myRefTwitts = myRefTwitts;
    }

    public StorageReference getStorageReference() {
        return storageReference;
    }

    public void setStorageReference(StorageReference storageReference) {
        this.storageReference = storageReference;
    }

    public void subirUser(User user){

        String formatEmail = user.getEmail().replace(".", "-");
        myRefUsers.child(formatEmail).setValue(user);
    }

    public void subirTwitt(Twitt twitt){
        String key = myRefTwitts.push().getKey();
        twitt.setId(key);
        myRefTwitts.child(key).setValue(twitt);
    }

    public void actualizarUser(User user){
        Map<String, Object> userMap = new HashMap<>();
        userMap.put("likeTwitts", user.getLikeTwitts());

        String formatEmail = user.getEmail().replace(".", "-");
        myRefUsers.child(formatEmail).updateChildren(userMap);
    }

    public static void actualizarLikesTwitt(Twitt twitt){
        Map<String, Object> twittMap = new HashMap<>();
        twittMap.put("usersLike", twitt.getUsersLike());

        if (twitt.getUsersLike() == null){
            myRefTwitts.child(twitt.getId()).setValue(twitt);
        }else {
            myRefTwitts.child(twitt.getId()).updateChildren(twittMap);
        }
    }

}
