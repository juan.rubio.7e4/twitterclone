package cat.itb.twitterclone.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User {
    private String alias;
    private String username;
    private String image;
    private String password;
    private String email;
    private Date birthDate;
    private List<Twitt> twitts = new ArrayList<>();
    private List<Twitt> likeTwitts = new ArrayList<>();

    public User() {
    }

    public User(String alias, String username, String email, String image, String password) {
        this.alias = alias;
        this.username = username;
        this.email = email;
        this.image = image;
        this.password = password;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Twitt> getTwitts() {
        return twitts;
    }

    public void setTwitts(List<Twitt> twitts) {
        this.twitts = twitts;
    }

    public List<Twitt> getLikeTwitts() {
        return likeTwitts;
    }

    public void setLikeTwitts(List<Twitt> likeTwitts) {
        this.likeTwitts = likeTwitts;
    }

    public void addLikeTwitt(Twitt twitt){
        this.likeTwitts.add(twitt);
    }

    public void removeLikeTwitt(Twitt twitt){
        this.likeTwitts.remove(twitt);
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
