package cat.itb.twitterclone.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Twitt {
    private String id;
    private String text;
    private Date date;
    private User user;
    private int message;
    private int retwitt;
    private List<String> usersLike;

    public Twitt() {
    }

    public Twitt(String text, Date date, User user) {
        this.text = text;
        this.date = date;
        this.user = user;
        this.message = 0;
        this.retwitt = 0;
        usersLike = new ArrayList<>();
    }

    public Twitt(String text, Date date, User user, int message, int retwitt) {
        this.text = text;
        this.date = date;
        this.user = user;
        this.message = message;
        this.retwitt = retwitt;
        usersLike = new ArrayList<>();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public int getRetwitt() {
        return retwitt;
    }

    public void setRetwitt(int retwitt) {
        this.retwitt = retwitt;
    }

    public List<String> getUsersLike() {
        return usersLike;
    }

    public void setUsersLike(List<String> usersLike) {
        this.usersLike = usersLike;
    }

    public boolean userLiked(User user) {
        String userName = user.getUsername();
        for (int i = 0; i < getUsersLike().size() ; i++) {
            if (getUsersLike().get(i).equals(userName)) {
                return true;
            }
        }
        return false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
